package example.spring_events.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.ContextStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AnnotationDrivenContextStartedListener {

    //@Async  -- use to make method asynchronous
    @EventListener
    public void handleContextStart(ContextStartedEvent event){
        log.info("Handling context started event.");
    }
}
